import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { GetterService } from '../getter.service';
import { IAdresses } from '../interface';

@Component({
  selector: 'app-important-adresses',
  templateUrl: './important-adresses.component.html',
  styleUrls: ['./important-adresses.component.scss']
})
export class ImportantAdressesComponent implements OnDestroy{
  public subscription: Subscription = new Subscription;
  constructor(private service: GetterService){}
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public adressesList: IAdresses[] = [];

  getAdresses(){
    this.subscription = this.service.getAdresses().subscribe((data:IAdresses[])=>{
      this.adressesList = data;
      console.log(data);
    })
  }

}
