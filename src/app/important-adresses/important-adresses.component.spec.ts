import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportantAdressesComponent } from './important-adresses.component';

describe('ImportantAdressesComponent', () => {
  let component: ImportantAdressesComponent;
  let fixture: ComponentFixture<ImportantAdressesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImportantAdressesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ImportantAdressesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
