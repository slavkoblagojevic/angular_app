//for important adresses
export interface IAdresses {
  id: number
  uid: string
  city: string
  street_name: string
  street_address: string
  secondary_address: string
  building_number: string
  mail_box: string
  community: string
  zip_code: string
  zip: string
  postcode: string
  time_zone: string
  street_suffix: string
  city_suffix: string
  city_prefix: string
  state: string
  state_abbr: string
  country: string
  country_code: string
  latitude: number
  longitude: number
  full_address: string
}

//for Profile-Data  
export interface IUser {
  id: number
  uid: string
  password: string
  first_name: string
  last_name: string
  username: string
  email: string
  avatar: string
  gender: string
  phone_number: string
  social_insurance_number: string
  date_of_birth: string
  employment: Employment
  address: Address
  credit_card: CreditCard
  subscription: Subscription
}

export interface Employment {
  title: string
  key_skill: string
}

export interface Address {
  city: string
  street_name: string
  street_address: string
  zip_code: string
  state: string
  country: string
  coordinates: Coordinates
}

export interface Coordinates {
  lat: number
  lng: number
}

export interface CreditCard {
  cc_number: string
}

export interface Subscription {
  plan: string
  status: string
  payment_method: string
  term: string
}

//for Personal-Data
export interface IPersonalDataObject {
  results: PersonalData[]
  info: Info
}

export interface PersonalData {
  gender: string
  name: Name
  location: Location
  email: string
  phone: string
}

export interface Name {
  title: string
  first: string
  last: string
}

export interface Location {
  street: Street
  city: string
  state: string
  country: string
  postcode: string
  coordinates: Coordinates
  timezone: Timezone
}

export interface Street {
  number: number
  name: string
}

export interface Coordinates {
  latitude: string
  longitude: string
}

export interface Timezone {
  offset: string
  description: string
}

export interface Info {
  seed: string
  results: number
  page: number
  version: string
}

  
  export type TParams = 'gender'| 'name' | 'location' | 'email' | 'phone';