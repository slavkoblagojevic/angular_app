import { Component, ElementRef, ViewChild, AfterViewInit, OnDestroy, OnInit } from '@angular/core'
import { Map, NavigationControl, Marker } from 'maplibre-gl'

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit, AfterViewInit, OnDestroy{
  map: Map | undefined;

  @ViewChild('map')
  private mapContainer!: ElementRef<HTMLElement>;

  constructor() { }

  ngOnInit(): void {

  }

  ngAfterViewInit() {
    const initialState = { lng: 20.461786, lat:44.814176, zoom: 7};
    
    this.map = new Map(
      {
        container: this.mapContainer.nativeElement,
        style: `https://api.maptiler.com/maps/streets-v2/style.json?key=OlwBRhm5Hj44yFIUXUMB`,
        center: [initialState.lng, initialState.lat],
        zoom: initialState.zoom
      });
    this.map.addControl(new NavigationControl({}), 'top-right');
    new Marker({color: "#FF0000"})
      .setLngLat([19.677224,46.097591])
      .addTo(this.map);
  }

  ngOnDestroy() {
    this.map?.remove();
  }
}
