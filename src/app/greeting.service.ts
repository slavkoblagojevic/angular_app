import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GreetingService {

  constructor() { }

  private dailyGreetingSubject: BehaviorSubject<string> = new BehaviorSubject<string>('');

  sendGreeting(message:string){
    this.dailyGreetingSubject.next(message);
  }

  getGreeting(){
   return this.dailyGreetingSubject.asObservable();
  }

}
