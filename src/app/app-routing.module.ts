import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { ErrorComponent } from './error/error.component';
import { ImportantAdressesComponent } from './important-adresses/important-adresses.component';
import { ProfileDataComponent } from './profile-data/profile-data.component';
import { SettingsComponent } from './settings/settings.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  {
    path: 'profile', component: ProfileComponent, children: [
      {
        path: 'tab1',
        component: ProfileDataComponent
      },
      {
        path: 'tab2',
        component: ImportantAdressesComponent
      },
      {
        path: '',
        redirectTo: 'tab1',
        pathMatch: 'full'
      }
    ]
  },
  { path: 'settings', component: SettingsComponent},
  { path: '', pathMatch: 'full', redirectTo: '/home' },
  { path: '**', component: ErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
