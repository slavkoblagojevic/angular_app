import { Component } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { ColorService } from '../color.service';
import { CookieService } from 'ngx-cookie-service';
import { MatInput } from '@angular/material/input';

@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss'],
})
export class ColorPickerComponent {
  public selectedColor: string = '#0d0303';
  constructor(
    private _bottomSheetRef: MatBottomSheetRef<ColorPickerComponent>,
    private colorService: ColorService,
    private cookieService: CookieService
  ) {}

  generateRandomColor() {
    const randomNum = Math.floor(Math.random() * 16777215).toString(16);
    this.selectedColor = '#' + randomNum;
    this.colorService.sendColor(this.selectedColor);
  }

  saveColor() {
    this.cookieService.set('color', this.selectedColor);
    console.log(this.cookieService.get('color'))
    this._bottomSheetRef.dismiss();
  }

  cancelColor() {
    this.colorService.sendColor(this.cookieService.get('color'));
    this._bottomSheetRef.dismiss();
  }

  inputColorManualy(input:HTMLInputElement){
    this.selectedColor = input.value;
    this.colorService.sendColor(this.selectedColor);
  }
}
