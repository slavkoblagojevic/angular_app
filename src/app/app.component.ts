import { Component, OnInit, ViewChild, AfterViewInit, ElementRef} from '@angular/core';
import { MatToolbar } from '@angular/material/toolbar';
import { ColorService } from './color.service';
import { CookieService } from 'ngx-cookie-service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit{
  
  constructor(private colorService: ColorService, private cookieService: CookieService, public translate: TranslateService){
    translate.addLangs(['EN-US', 'SR-Lat', 'SR-Cyr']);
    translate.setDefaultLang('EN-US');
    const language: any = localStorage.getItem('language')
    if(language){
      translate.use(language);
    }
    this.checkCookieColor()
  }

  switchLang(lang:string){
    this.translate.use(lang);
    localStorage.setItem("language", lang);
  }

  checkCookieColor(){
    const retrievedColor = this.cookieService.get('color');
    if(retrievedColor){
      console.log(retrievedColor)
      this.colorService.sendColor(retrievedColor)
    }
  }
  
  ngOnInit(): void {
    let toolbar:HTMLElement | null = document.querySelector('.myToolbar');
    this.colorService.getColor().subscribe(res =>{
      console.log(res)
      toolbar!.style.backgroundColor = res;
    });
    
  }

  ngAfterViewInit(): void {}
 
}
