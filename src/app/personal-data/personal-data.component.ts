import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { Subscription } from 'rxjs';
import { GetterService } from '../getter.service';
import { PersonalData, IPersonalDataObject, TParams } from '../interface';

@Component({
  selector: 'app-personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: ['./personal-data.component.scss'],
})
export class PersonalDataComponent implements OnInit, OnDestroy {
  constructor(private service: GetterService) {
    this.onSave()
  }
  
  checkedParams: TParams[] = ['gender', 'name', 'location', 'email', 'phone'];
  personalData?: PersonalData;
  paramsSaved: TParams[] =[];
  public subscription: Subscription = new Subscription;

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onCheckBoxChange(event: MatCheckboxChange, value: TParams) {
    if (event.checked) {
      this.checkedParams.push(value);
    } else {
      let index = this.checkedParams.indexOf(value);
      this.checkedParams.splice(index, 1);
    }
    console.log(this.checkedParams);
  }

  onSave(){
    this.paramsSaved = [...this.checkedParams]
    console.log(this.paramsSaved)
    if(this.paramsSaved.length == 0) return;
    this.subscription = this.service.getPersonalData(this.paramsSaved).subscribe((response:IPersonalDataObject) =>{
      console.log(response);
      this.personalData = response.results[0];
    })
  }
}
