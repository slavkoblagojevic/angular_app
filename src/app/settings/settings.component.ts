import { Component } from '@angular/core';
import { GreetingService } from '../greeting.service';
import {
  MatBottomSheet,
  MatBottomSheetConfig,
} from '@angular/material/bottom-sheet';
import { ColorPickerComponent } from '../color-picker/color-picker.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent {
  article: { id: number; title: string; content: string } | null = {
    id: 5444,
    title: 'ng-template example',
    content: 'If article is null ng-template will be rendered, otherwise title will be rendered',
  };
  numbers:number[]=[1,2,3,4];
  dailyGreeting: string = '';
  config: MatBottomSheetConfig = {
    hasBackdrop: false,
  };
  constructor(
    private service: GreetingService,
    private _bottomSheet: MatBottomSheet
  ) {}
  saveGreeting() {
    this.service.sendGreeting(this.dailyGreeting);
  }

  openBottomSheet() {
    this._bottomSheet.open(ColorPickerComponent, this.config);
  }
}
