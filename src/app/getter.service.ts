import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IAdresses, IPersonalDataObject, IUser } from './interface';

@Injectable({
  providedIn: 'root'
})
export class GetterService {
  

  private url:string = 'https://random-data-api.com/api/v2/users?size=';
  private urlPersonalData:string = 'https://randomuser.me/api/?inc=';
  private urlAdresses:string = 'https://random-data-api.com/api/v2/addresses?size=';

  constructor(private http: HttpClient) { }


  getUsers():Observable<IUser[]>{
    let randomNum: number = Math.floor(Math.random()*10) + 3; //dodajem 3 zbog opsega od 3-12 u tekstu zadatka(default 0-9)
    return this.http.get<IUser[]>(this.url + randomNum.toString());
  }

  getAdresses():Observable<IAdresses[]>{
    let randomNum: number = Math.floor(Math.random()*10) + 1;
    return this.http.get<IAdresses[]>(this.urlAdresses + randomNum.toString());
  }

  getPersonalData(params:Array<string>):Observable<IPersonalDataObject>{
    let newUrl: string = this.urlPersonalData;
    for(let i=0; i<params.length; i++){
      newUrl = newUrl + params[i] + ','; 
    }
    return this.http.get<IPersonalDataObject>(newUrl);
  }

}
