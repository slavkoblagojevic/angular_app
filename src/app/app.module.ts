import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule} from '@angular/material/input';

import { MapComponent } from './map/map.component';
import { ErrorComponent } from './error/error.component';
import { ImportantAdressesComponent } from './important-adresses/important-adresses.component';
import { ProfileDataComponent } from './profile-data/profile-data.component';
import { ProfileComponent } from './profile/profile.component';
import { HomeComponent } from './home/home.component';
import { PersonalDataComponent } from './personal-data/personal-data.component';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { SettingsComponent } from './settings/settings.component';
import { FormsModule } from '@angular/forms';
import { MatBottomSheetModule} from '@angular/material/bottom-sheet';
import { ColorPickerComponent } from './color-picker/color-picker.component';
import { CookieService } from 'ngx-cookie-service';
import { ColorPickerModule } from 'ngx-color-picker';
import { MatMenuModule} from '@angular/material/menu';
import { TranslateLoader, TranslateModule} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProfileComponent,
    MapComponent,
    ErrorComponent,
    ImportantAdressesComponent,
    ProfileDataComponent,
    PersonalDataComponent,
    SettingsComponent,
    ColorPickerComponent,
    
  ],
  imports: [
    BrowserModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient],
      }
  }),
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatButtonModule,
    MatListModule,
    MatTabsModule,
    RouterModule,
    HttpClientModule,
    MatCardModule,
    MatCheckboxModule,
    MatInputModule,
    FormsModule,
    MatBottomSheetModule,
    ColorPickerModule,
    MatMenuModule
  ],
  providers: [CookieService],
  bootstrap: [AppComponent],
})
export class AppModule {}
