import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { GetterService } from '../getter.service';
import { IUser } from '../interface';

@Component({
  selector: 'app-profile-data',
  templateUrl: './profile-data.component.html',
  styleUrls: ['./profile-data.component.scss'],
})
export class ProfileDataComponent implements OnInit, OnDestroy {

  public usersList: IUser[] = [];
  firsttime: any;
  public subscription: Subscription = new Subscription;
  constructor(private service: GetterService) {}

  ngOnInit(): void {
    this.greeting();
    this.getUsers();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  greeting(){
    if(!localStorage.getItem('firsttime')){
      this.firsttime = 'true';
      localStorage.setItem('firsttime', 'true')
    }else{
      this.firsttime = 'false'
      localStorage.setItem('firsttime', 'false');
    }
    this.firsttime = localStorage.getItem('firsttime');
  }

  getUsers(){
    this.subscription = this.service.getUsers().subscribe((data: IUser[]) => {
      console.log(data)
      this.usersList = data;
    });
  }

}
