import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { GreetingService } from '../greeting.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  dailyGreeting?: string;
  subscription = new Subscription();
  constructor(private service: GreetingService) {}
  ngOnInit(): void {
    this.getGreeting();
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  getGreeting(){
    this.subscription = this.service.getGreeting().subscribe((res:string) =>{
      this.dailyGreeting = res;
    })
  }
}
